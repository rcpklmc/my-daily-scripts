# run with python3
import os
from socket import *
import platform

if platform.system() == "Linux":
	os.system("clear")
elif platform.system() == "Windows":
	os.system("cls")
else:
	os.system("clear")
print("====================================================")
print("============= Port Scanning(1-65535) ===============")
print("====================================================")

if __name__ == '__main__':
	
	enter_target_IP = input("Enter IP address for scanning : ")
	getHostByName = gethostbyname(enter_target_IP)
	
	for port in range(1,65535):
		sock = socket(AF_INET,SOCK_STREAM)	# IPv4 , TCP
		res = sock.connect_ex((getHostByName,port))
		
		if (res == 0):
			print(port , " is open")
		sock.close()
print("====================================================")
print("Scanning is finished :)")
